#!/bin/python
""" A GUI application for an interactive demonstration of Graham's algorithm. """

import tkinter as tk
import tkinter.font as tkFont
from collections import namedtuple
from enum import Enum, auto
from random import randint

from solver import Graham_Solver, Step_Action

Point = namedtuple('Point', ['x', 'y'])
""" Type alias for a common named tuple. """


def point_to_string(p):
    """ Returns a string representing a given point. """
    return "({0:d}, {1:d})".format(int(p.x), int(p.y))


WHITE = "#FFFFFF"
BLACK = "#000000"
GREEN = "#00AB6B"
BLUE = "#0080FF"
RED = "#ED2939"

POINT_RADIUS = 5

SEPARATOR = "━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\n"

PRETTY_CAP = "┌──────────────────┐\n"
PRETTY_DIV = "├──────────────────┤\n"
PRETTY_CUP = "└──────────────────┘\n"
PRETTY_WALL = "│"


class Phase(Enum):
    """ Enumerator Class describing the current state of the demo window. """
    DRAW = auto()
    """ User input phase, points being placed. """
    STEP = auto()
    """ Compute step phase, hull being traced. """


class Demo_Window:
    """ The main demonstration window, providing the interactive GUI for the
    algorithm's execution. """

    # pylint: disable=too-many-instance-attributes

    def __init__(self, root_window):
        """ Basic constructor, creating all of the GUI assets and hooking into
        the provided root. """

        self.phase = Phase.DRAW
        self.points = []
        self.solver = None
        self.history = None
        self.rejected = None
        self.step_count = 0
        self.algo_marks = None

        root_window.bind("<Left>", self.on_left_key)
        root_window.bind("<Right>", self.on_right_key)
        root_window.bind("<Return>", self.on_enter_key)
        root_window.bind("<space>", self.on_space_key)
        root_window.bind("<BackSpace>", self.on_backspace_key)
        root_window.bind("<Home>", self.on_home_key)

        self.frame = tk.Frame(root_window)
        self.frame.grid_rowconfigure(1, weight=1)
        self.frame.grid_columnconfigure(0, weight=1)
        self.frame.pack(expand=1, fill=tk.BOTH)

        self.canvas = tk.Canvas(self.frame, bg=WHITE)
        self.canvas.bind("<Button-1>", self.on_canvas_left_click)
        self.canvas.bind("<Button-3>", self.on_canvas_right_click)
        self.canvas.grid(row=0, column=0, rowspan=3, sticky=tk.NSEW)

        self.points_panel_label = tk.Label(self.frame, text="Points")
        self.points_panel_label.grid(row=0, column=1, sticky=tk.NSEW)
        self.stack_panel_label = tk.Label(self.frame, text="Stack")
        self.stack_panel_label.grid(row=0, column=2, sticky=tk.NSEW)
        self.history_panel_label = tk.Label(self.frame, text="History")
        self.history_panel_label.grid(row=0, column=3, sticky=tk.NSEW)

        self.points_panel = tk.Text(self.frame, state=tk.DISABLED, width=20)
        self.points_panel.grid(row=1, column=1, sticky=tk.NSEW)
        self.stack_panel = tk.Text(self.frame, state=tk.DISABLED, width=20)
        self.stack_panel.grid(row=1, column=2, sticky=tk.NSEW)
        self.history_panel = tk.Text(self.frame, state=tk.DISABLED, width=20)
        self.history_panel.grid(row=1, column=3, sticky=tk.NSEW)

        self.report_panel = tk.Text(
            self.frame, state=tk.DISABLED, width=60, height=10, wrap=tk.WORD)
        self.report_panel.grid(row=2, column=1, columnspan=3, sticky=tk.EW)

        self.algo_panel_label = tk.Label(self.frame, text="Algorithm View")
        self.algo_panel_label.grid(row=0, column=4, sticky=tk.NSEW)
        self.algo_panel = tk.Text(self.frame, state=tk.NORMAL, width=40)
        self.init_algo_text()
        self.algo_panel.config(state=tk.DISABLED)
        self.algo_panel.grid(row=1, column=4, rowspan=2, sticky=tk.NS)

        self.back_button = tk.Button(self.frame,
                                     text="Back",
                                     command=self.on_back_button,
                                     state=tk.DISABLED)
        self.back_button.grid(row=3, column=1, sticky=tk.NSEW)

        self.step_button = tk.Button(self.frame,
                                     text="Start",
                                     command=self.on_step_button)
        self.step_button.grid(row=3, column=2, sticky=tk.NSEW)

        self.reset_button = tk.Button(self.frame,
                                      text="Reset",
                                      command=self.on_reset_button)
        self.reset_button.grid(row=3, column=3, sticky=tk.NSEW)

        self.redraw_design_canvas()
        self.update_stats()

    def init_algo_text(self):
        """ Set up the algorithm panel with all our text. """
        self.algo_panel.config(font="Courier")
        self.algo_panel.tag_config("highlight",
                                   background="darkSeaGreen1")
        self.algo_panel.tag_config("keyword",
                                   font=tkFont.Font(family="Courier",
                                                    weight="bold"))
        self.algo_panel.tag_config("var",
                                   font=tkFont.Font(family="Courier",
                                                    slant="italic"))

        def output(*content):
            """ Quick helper function to print content to panel. """
            for m in content:
                if isinstance(m, tuple):
                    self.algo_panel.insert(tk.END, m[0], m[1])
                else:
                    self.algo_panel.insert(tk.END, m)

        output(
            ("algorithm", "keyword"), " graham_scan ", ("is\n", "keyword"),
            "┊    ", ("input", "keyword"), ":  Pointset ", ("P\n", "var"),
            "┊    ", ("output", "bold"), ": Hull ", ("H\n", "var"),
            "┊  ", ("let ", "keyword"), ("P_y_sort", "var"), " be a Pointset\n",  # noqa
            "┊      ", ("P_p_sort", "var"), " be a Pointset\n",
            "┊      ", ("O", "var"), " be a Point\n",
            "┊  ", ("H", "var"), " ← []\n",
            "┊  ", ("P_y_sorted", "var"), " ← sort ", ("P", "var"), " by ↓ y-coord\n",  # noqa
            "┊  ", ("O", "var"), " ← ", ("P_y_sorted", "var"), "[0]\n",
            "┊  ", ("P_p_sorted", "var"), " ← sort (", ("P", "var"), "∖", ("O", "var"), " by ↓ φ(", ("O", "var"), ",", ("p", "var"), ")\n",  # noqa
            "┊  ", ("H", "var"), ".push(", ("O", "var"), ")\n",
            "┊  ", ("H", "var"), ".push(", ("P_p_sorted", "var"), ".pop_first())\n",  # noqa
            "┊  ", ("for each", "keyword"), " Point ", ("p", "var"), (" in ", "keyword"), ("P_p_sorted", "var"), ":\n",  # noqa
            "┊  ┊  ", ("while not", "keyword"), " turns_right(", ("H", "var"), "[-2],\n",  # noqa
            "┊  ┊                        ", ("H", "var"), "[-1],\n",
            "┊  ┊                        ", ("p", "var"), "):\n",
            "┊  ┊  ┊  ", ("H", "var"), ".pop()\n",
            "┊  ┊  ", ("H", "var"), ".push(p)\n",
            "┊  ", ("return ", "keyword"), ("H\n", "var")
        )

    def update_algorithm_highlight(self):
        """ Clears the algorithm highlighting and updates new highlighting as
        necessary."""
        self.algo_panel.config(state=tk.NORMAL)
        if self.algo_marks != None:
            for mark in self.algo_marks:
                self.algo_panel.tag_remove("highlight",
                                           "%d.0" % mark,
                                           "%d.0" % (mark + 1))

        self.algo_marks = None
        if self.phase == Phase.STEP:
            if len(self.history) == 0:
                self.algo_marks = range(7, 13)
            else:
                if self.history[-1][0] == Step_Action.CHECK:
                    self.algo_marks = [13]
                elif self.history[-1][0] == Step_Action.REMOVE:
                    self.algo_marks = range(14, 18)
                elif self.history[-1][0] == Step_Action.COMMIT:
                    self.algo_marks = [18]
                elif self.history[-1][0] == Step_Action.FINALIZE:
                    self.algo_marks = [19]

            for mark in self.algo_marks:
                self.algo_panel.tag_add("highlight",
                                        "%d.0" % mark,
                                        "%d.0" % (mark + 1))
        self.algo_panel.config(state=tk.DISABLED)

    def report(self, *message):
        """ Prints a message out to the report panel(clearing the prior
        report). """

        self.report_panel.config(state=tk.NORMAL)
        self.report_panel.delete(1.0, tk.END)
        for m in message:
            self.report_panel.insert(tk.END, m)
        self.report_panel.config(state=tk.DISABLED)

    def update_stats(self):
        """ Draws updates for all output panels. """

        self.draw_stats_points()
        self.draw_stats_stack()
        self.draw_stats_history()

    def draw_stats_points(self):
        """ Updates the point list visualization panel to reflect the current
        points in the point set(DRAW) or the remaining points in the sorted
        point set(STEP). """

        self.points_panel.config(state=tk.NORMAL)
        self.points_panel.delete(1.0, tk.END)
        self.points_panel.insert(tk.END, PRETTY_CAP)
        points = None
        if self.phase == Phase.DRAW:
            points = self.points
        elif self.phase == Phase.STEP:
            points = self.solver.points

        for i, p in enumerate(points):
            if i != 0:
                self.points_panel.insert(tk.END, PRETTY_DIV)
            self.points_panel.insert(tk.END,
                                     "{0}{1:^18}{0}\n".format(PRETTY_WALL,
                                                              point_to_string(p)))
        self.points_panel.insert(tk.END, PRETTY_CUP)
        self.stack_panel.see(tk.END)
        self.points_panel.config(state=tk.DISABLED)

    def draw_stats_stack(self):
        """ Updates the stack visualization panel to reflect the currently
        working hull. """

        self.stack_panel.config(state=tk.NORMAL)
        self.stack_panel.delete(1.0, tk.END)
        if self.phase == Phase.STEP:
            self.stack_panel.insert(tk.END, PRETTY_CAP)
            for i, p in enumerate(self.solver.working_hull):
                if i != 0:
                    self.stack_panel.insert(tk.END, PRETTY_DIV)
                self.stack_panel.insert(tk.END,
                                        "{0}{1:^18}{0}\n".format(PRETTY_WALL,
                                                                 point_to_string(p)))
            self.stack_panel.insert(tk.END, PRETTY_CUP)
        self.stack_panel.see(tk.END)
        self.stack_panel.config(state=tk.DISABLED)

    def draw_stats_history(self):
        """ Updates the history output panel to reflect the current history
        chain. """

        self.history_panel.config(state=tk.NORMAL)
        self.history_panel.delete(1.0, tk.END)
        if self.phase == Phase.STEP:
            self.history_panel.insert(tk.END, PRETTY_CAP)
            for i, step in enumerate(self.history):
                if i != 0:
                    self.history_panel.insert(tk.END, PRETTY_DIV)
                out = None
                if self.solver.finished and i == (len(self.history) - 1):
                    out = "Complete!"
                else:
                    out = "{0} {1}".format(step[0].name,
                                           point_to_string(step[1]))
                self.history_panel.insert(tk.END,
                                          "{0}{1:^18}{0}\n".format(PRETTY_WALL,
                                                                   out))
            self.history_panel.insert(tk.END, PRETTY_CUP)
        self.history_panel.see(tk.END)
        self.history_panel.config(state=tk.DISABLED)

    def redraw_design_canvas(self):
        """ Clear the canvas and redraw all points for design mode. """

        self.canvas.delete(tk.ALL)

        # Mode Label
        self.canvas.create_text(5, 5, font=("", 10, "bold"),
                                anchor=tk.NW, text="Draw")

        # Draw Points
        for p in self.points:
            self.canvas.create_oval(
                p.x - POINT_RADIUS, p.y - POINT_RADIUS,
                p.x + POINT_RADIUS, p.y + POINT_RADIUS,
                fill=BLACK)

    def redraw_step_canvas(self):
        """ Clear the canvas and redraw all our points, as well as the working
        hull for the step mode. """

        self.canvas.delete(tk.ALL)

        # Mode Label
        self.canvas.create_text(5, 5, font=("", 10, "bold"),
                                anchor=tk.NW, text="Visualize")

        # Draw Points
        for p in self.points:
            self.canvas.create_oval(
                p.x - POINT_RADIUS, p.y - POINT_RADIUS,
                p.x + POINT_RADIUS, p.y + POINT_RADIUS,
                fill=BLACK)

        # Overdraw colored points where applicable
        if self.solver.current != None:
            # The current point counts as the convex hull.
            self.canvas.create_polygon(
                [self.solver.current,
                 self.solver.working_hull[-1],
                 self.solver.working_hull[0]],
                fill="slateGray1")
            # If we just removed a point, show the triangle.
            if self.history[-1][0] == Step_Action.REMOVE:
                self.canvas.create_polygon(
                    [self.solver.current,
                     self.history[-1][1],
                     self.solver.working_hull[-1]], fill="paleGreen")

            self.canvas.create_oval(
                self.solver.current.x - POINT_RADIUS,
                self.solver.current.y - POINT_RADIUS,
                self.solver.current.x + POINT_RADIUS,
                self.solver.current.y + POINT_RADIUS,
                fill=GREEN)
            # Draw in a line to save a duplicate branch
            pair = self.solver.working_hull[-1]
            self.canvas.create_line(
                pair.x, pair.y,
                self.solver.current.x,
                self.solver.current.y,
                fill=GREEN)

        if len(self.history) > 0:
            # Make the hull look nice
            if len(self.solver.working_hull) >= 3:
                self.canvas.create_polygon(
                    self.solver.working_hull, fill="lightBlue1")

        for p in self.rejected:
            self.canvas.create_oval(
                p.x - POINT_RADIUS, p.y - POINT_RADIUS,
                p.x + POINT_RADIUS, p.y + POINT_RADIUS,
                fill=RED)
        for p in self.solver.working_hull:
            self.canvas.create_oval(
                p.x - POINT_RADIUS, p.y - POINT_RADIUS,
                p.x + POINT_RADIUS, p.y + POINT_RADIUS,
                fill=BLUE)

        # Draw in lines
        for i, p in enumerate(self.solver.working_hull[1:]):
            pair = self.solver.working_hull[i]
            self.canvas.create_line(pair.x, pair.y, p.x, p.y, fill=BLUE)

        if self.solver.finished:
            start = self.solver.working_hull[0]
            end = self.solver.working_hull[-1]
            self.canvas.create_line(start.x, start.y, end.x, end.y, fill=BLUE)

    def on_canvas_left_click(self, click_event):
        """ Callback to be executed on canvas < BUTTON - 1 > event. """

        if self.phase == Phase.DRAW:
            new_point = Point(x=self.canvas.canvasx(click_event.x),
                              y=self.canvas.canvasy(click_event.y))
            if new_point in self.points:
                self.report("Ignoring duplicate point.")
                return

            self.report("Added point at cursor (",
                        new_point.x, ",",
                        new_point.y, ")")
            self.points.append(new_point)
            self.redraw_design_canvas()
            self.update_stats()

            self.back_button.config(state=tk.NORMAL)

    def on_canvas_right_click(self, click_event):
        """ Callback to be executed on canvas < BUTTON - 3 > event. """

        if self.phase == Phase.DRAW:
            if len(self.points) == 0:
                return
            request_point = Point(x=self.canvas.canvasx(click_event.x),
                                  y=self.canvas.canvasy(click_event.y))
            item = self.canvas.find_closest(request_point.x, request_point.y)
            item_coord = self.canvas.coords(item)
            removed_point = Point(item_coord[0] + POINT_RADIUS,
                                  item_coord[1] + POINT_RADIUS)
            self.points.remove(removed_point)
            self.report("Removed nearest point at (",
                        removed_point.x, ",",
                        removed_point.y, ")")

            self.redraw_design_canvas()
            self.update_stats()

            if len(self.points) == 0:
                self.back_button.config(state=tk.DISABLED)

    def enter_draw_phase(self):
        """ Set the demo to the DRAW phase(does not assume prior phase was
        STEP). """
        self.step_button.config(state=tk.NORMAL, text="Start")
        self.phase = Phase.DRAW
        self.solver = None
        self.rejected = None
        self.history = None
        self.step_count = 0
        self.update_algorithm_highlight()
        self.redraw_design_canvas()
        self.update_stats()

    def enter_step_phase(self):
        """ Set the demo to the STEP phase(does nto assume prior phase was
        DRAW). """
        if len(self.points) < 3:
            self.report("Need more points!")
            return
        self.phase = Phase.STEP
        self.solver = Graham_Solver(self.points)
        self.rejected = []
        self.history = []
        self.step_count = 0
        self.step_button.config(text="Step")
        self.report(SEPARATOR,
                    "❰ Initializing Convex Hull ❱\n",
                    "⚫ The p with the lowest y-coord is our polar origin: ",
                    point_to_string(self.solver.origin), "\n"
                    "⚫ We add this and the p with the lowest φ to the hull:",
                    ''.join([(" " + point_to_string(p))
                             for p in self.solver.working_hull]), "\n",
                    "⚫ The remaining points are sorted from lowest to highest φ:",
                    ''.join([(" " + point_to_string(p))
                             for p in self.points]), "\n",
                    SEPARATOR)
        self.update_algorithm_highlight()
        self.redraw_step_canvas()
        self.update_stats()

    def step(self):
        """ Move the state of the solver forward one step. """
        if self.phase == Phase.DRAW:
            self.enter_step_phase()
        elif self.phase == Phase.STEP:
            if self.solver.finished:
                # self.report("Already finished!")
                return

            step_action = self.solver.step()
            if step_action[0] == Step_Action.CHECK:
                self.report("Attempting to add ",
                            point_to_string(step_action[1]),
                            " to convex hull.\n\n",
                            "We'll need to remove interior points from the "
                            "stack.")
            elif step_action[0] == Step_Action.REMOVE:
                self.step_count += 1
                self.rejected.append(step_action[1])
                self.report("The triangle formed by the top two points on ",
                            "the stack and the new point is outside the hull. ",
                            "The last point must be interior.\n\n",
                            "Removing bad point ",
                            point_to_string(step_action[1]),
                            " from convex hull.\n\n",
                            "Now we check the next point on the stack.")
            elif step_action[0] == Step_Action.COMMIT:
                self.step_count += 1
                self.report("The triangle formed by the top two points on the ",
                            "stack and the new point lies within the hull. ",
                            "The new hull is now considered safe.\n\n",
                            "Committed ",
                            point_to_string(step_action[1]),
                            " to convex hull.")
            elif step_action[0] == Step_Action.FINALIZE:
                self.report(SEPARATOR,
                            "❰ Convex Hull Generated ❱\n",
                            "⚫ Points on hull:",
                            ''.join([(" " + point_to_string(p))
                                     for p in step_action[1]]),
                            "\n",
                            "⚫ Steps taken:", self.step_count, "\n",
                            SEPARATOR)
                self.step_button.config(state=tk.DISABLED)

            self.history.append(step_action)

            self.update_algorithm_highlight()
            self.redraw_step_canvas()
            self.update_stats()

            self.back_button.config(state=tk.NORMAL)

    def backtrack(self):
        """ Move the state of the solver back one step. """
        if len(self.history) == 0:
            # self.report("Already at the beginning!")
            return

        prior_action = self.history.pop(-1)
        if prior_action[0] == Step_Action.CHECK:
            self.report("No longer looking at point (",
                        prior_action[1].x, ",",
                        prior_action[1].y, ")")
        elif prior_action[0] == Step_Action.COMMIT:
            self.report("Reverting inclusion of point (",
                        prior_action[1].x, ",",
                        prior_action[1].y, ")")
        elif prior_action[0] == Step_Action.REMOVE:
            self.report("Reverted removal of interior point (",
                        prior_action[1].x, ",",
                        prior_action[1].y, ")")
            self.rejected.remove(prior_action[1])
        elif prior_action[0] == Step_Action.FINALIZE:
            self.report("Reverted Convex Hull completion.")
        self.solver.backtrack(prior_action)

        self.update_algorithm_highlight()
        self.redraw_step_canvas()
        self.update_stats()

        self.step_button.config(state=tk.NORMAL)
        if len(self.history) == 0:
            self.back_button.config(state=tk.DISABLED)

    def reset(self):
        """ Reset the phase, clearing all points from DRAW and sending STEP back
        into DRAW. """

        if self.phase == Phase.DRAW:
            self.back_button.config(state=tk.DISABLED)
            self.points = []
            self.redraw_design_canvas()
            self.update_stats()
            self.report("Removed all existing points.")
        else:
            self.enter_draw_phase()
            self.update_stats()
            self.report(SEPARATOR,
                        "❰ Cancelled Stepping ❱\n",
                        "Ready to draw!\n",
                        SEPARATOR)

    def generate_points(self, count=5):
        """ Generate and place a few random points on the canvas. """

        if self.phase == Phase.DRAW:
            self.report("Generating 5 random points...")
            for _ in range(0, count):
                self.points.append(
                    Point(x=randint(0, self.canvas.winfo_width()),
                          y=randint(0, self.canvas.winfo_height())))

            self.redraw_design_canvas()
            self.update_stats()

            self.back_button.config(state=tk.NORMAL)

    def on_left_key(self, _):
        """ Left arrow key callback to move state of solver backward one step. """
        if self.phase == Phase.STEP:
            self.backtrack()

    def on_space_key(self, _):
        """ Space key callback to move state of solver forward one step. """
        if self.phase == Phase.DRAW:
            self.enter_step_phase()
        elif self.phase == Phase.STEP:
            self.step()

    def on_right_key(self, _):
        """ Right arrow key callback to move state of solver forward one step. """
        if self.phase == Phase.STEP:
            self.step()

    def on_enter_key(self, _):
        """ Enter key callback to commit drawing phase into step phase. """
        if self.phase == Phase.DRAW:
            self.enter_step_phase()

    def on_backspace_key(self, _):
        """ Backspace key callback to reset the current phase of execution. """
        self.reset()

    def on_home_key(self, _):
        """ Home key callback to randomly generate some points. """
        self.generate_points()

    def on_back_button(self):
        """ Callback to be executed when "Back" button is pressed by user to
        remove last entered point in DRAW phase or move solver backward one
        step in STEP phase. """
        if self.phase == Phase.DRAW:
            del self.points[-1]
            self.redraw_design_canvas()
        elif self.phase == Phase.STEP:
            self.backtrack()

    def on_step_button(self):
        """ Callback to be executed when the provided "Start" / "Step" button
        is pressed by user to move solver forward one step. """
        self.step()

    def on_reset_button(self):
        """ Callback to be executed when the provided "Reset" button is pressed. """
        self.reset()


root = tk.Tk()
demo = Demo_Window(root)
root.mainloop()
