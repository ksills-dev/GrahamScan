""" Contains logic and class definition for a Graham algorithm Solver. """

from math import atan2
from functools import partial
from enum import Enum, auto


def angle(p0, p1):
    """ Finds the angle between two given points. """
    x = p0.x - p1.x
    y = p0.y - p1.y
    return atan2(y, x)


def distance(p0, p1):
    """ Finds the Euclidean distance between two points. """
    x = p0.x - p1.x
    y = p0.y - p1.y
    return x**2 + y**2


def turns_right(p, q, s):
    """ Determines if the p ->* s traversal  is a right turn. """
    temp = (q.x - p.x) * (s.y - p.y) - (q.y - p.y) * (s.x - p.x)
    if temp <= 0:
        return True
    else:
        return False


class Step_Action(Enum):
    """ The action taken in a single step of Graham's algorithm. """
    CHECK = auto()
    """ Begin checking a new point. """
    REMOVE = auto()
    """ The prior point in the convex hull is no longer included. """
    COMMIT = auto()
    """ The current point being checked is admitted into the working hull. """
    FINALIZE = auto()
    """ The hull has been evaluated completely. """


class Graham_Solver:
    """ Step-by-step solver class for Graham's Convex Hull algorithm.

    One-time use, destroy and re-initialize to solve new hulls.

    Members:
    * self.origin
    * self.points
    * self.working_hull
    * self.finished
    """

    def __init__(self, points):
        """ Constructor, initializes a stack given the point set and
        prepares for step execution. """

        assert len(points) >= 3, "No 2D hull exists for less than 3 points."

        self.origin = points[0]
        for p in points:
            if p.y < self.origin.y:
                self.origin = p

        filtered = [p for p in points if p != self.origin]
        self.points = sorted(filtered, key=partial(angle, self.origin))
        self.working_hull = [self.origin, self.points[0]]
        self.points = self.points[1:]

        self.finished = False
        self.current = None

    def backtrack(self, prior_action):
        """ Backtracks the solver one step, given the prior action it had
        committed. """
        if prior_action[0] == Step_Action.CHECK:
            self.points.insert(0, self.current)
            self.current = None
        elif prior_action[0] == Step_Action.COMMIT:
            self.current = self.working_hull.pop(-1)
        elif prior_action[0] == Step_Action.REMOVE:
            self.working_hull.append(prior_action[1])
        elif prior_action[0] == Step_Action.FINALIZE:
            self.finished = False

    def step(self):
        """ Takes a single step for the algorithm.
        Returns:
        * (CHECK, p) - Begin analyzing inclusiveness of point p.
        * (REMOVE, p) - Remove false-hull point p.
        * (COMMIT, p) - Include point p into working hull.
        * (FINALIZE, [p_set]) - Hull complete, made up of set of points p_set.
        """
        assert self.finished == False, "Can't take a step on a complete hull."

        if self.current != None:
            if (len(self.working_hull) >= 2 and
                    turns_right(self.working_hull[-2],
                                self.working_hull[-1],
                                self.current)):
                removed = self.working_hull[-1]
                del self.working_hull[-1]
                return (Step_Action.REMOVE, removed)
            else:
                committed = self.current
                self.current = None
                self.working_hull.append(committed)
                return (Step_Action.COMMIT, committed)

        else:
            if len(self.points) == 0:
                self.finished = True
                return (Step_Action.FINALIZE, self.working_hull[:])

            self.current = self.points.pop(0)
            return (Step_Action.CHECK, self.current)
